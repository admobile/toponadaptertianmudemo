package com.tianmu.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.anythink.banner.api.ATBannerListener;
import com.anythink.banner.api.ATBannerView;
import com.anythink.core.api.ATAdInfo;
import com.anythink.core.api.AdError;

/**
 * @author oumatsumatsu
 */
public class BannerAdActivity extends AppCompatActivity {

    private FrameLayout flContainer;
    private ATBannerView mBannerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_ad);
        flContainer = findViewById(R.id.banner_ad_container);

        ViewGroup.LayoutParams layoutParams = flContainer.getLayoutParams();
        layoutParams.width = getResources().getDisplayMetrics().widthPixels;
        layoutParams.height = (int) (getResources().getDisplayMetrics().heightPixels * 0.3);


        mBannerView = new ATBannerView(this);
        mBannerView.setPlacementId("b65f9703e88c84");
        //定一个宽度值，比如屏幕宽度
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        mBannerView.setLayoutParams(new FrameLayout.LayoutParams(width, height));
        flContainer.addView(mBannerView);
        mBannerView.setBannerAdListener(new ATBannerListener() {
            @Override
            public void onBannerLoaded() {
                Log.e("BannerAdActivity", "onBannerLoaded");
            }

            @Override
            public void onBannerFailed(AdError adError) {
                //注意：禁止在此回调中执行广告的加载方法进行重试，否则会引起很多无用请求且可能会导致应用卡顿
                //AdError，请参考 https://docs.toponad.com/#/zh-cn/android/android_doc/android_test?id=aderror
                Log.e("BannerAdActivity", "onBannerFailed:" + adError.getFullErrorInfo());
            }

            @Override
            public void onBannerClicked(ATAdInfo atAdInfo) {
                Log.e("BannerAdActivity", "onBannerClicked");
            }

            @Override
            public void onBannerShow(ATAdInfo atAdInfo) {
                Log.e("BannerAdActivity", "onBannerShow");
                //ATAdInfo可区分广告平台以及获取广告平台的广告位ID等
                //请参考 https://docs.toponad.com/#/zh-cn/android/android_doc/android_sdk_callback_access?id=callback_info

            }

            @Override
            public void onBannerClose(ATAdInfo atAdInfo) {
                Log.e("BannerAdActivity", "onBannerClose");
                if (mBannerView != null && mBannerView.getParent() != null) {
                    ((ViewGroup) mBannerView.getParent()).removeView(mBannerView);
                }
            }

            @Override
            public void onBannerAutoRefreshed(ATAdInfo atAdInfo) {
                Log.e("BannerAdActivity", "onBannerAutoRefreshed");
            }

            @Override
            public void onBannerAutoRefreshFail(AdError adError) {
                //AdError：https://docs.toponad.com/#/zh-cn/android/android_doc/android_test?id=aderror
                Log.e("BannerAdActivity", "onBannerAutoRefreshFail:" + adError.getFullErrorInfo());
            }
        });
        mBannerView.loadAd();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBannerView != null) {
            mBannerView.destroy();
        }
    }
}
