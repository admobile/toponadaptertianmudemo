package com.tianmu.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;

import com.anythink.core.api.ATAdInfo;
import com.anythink.core.api.AdError;
import com.anythink.nativead.api.ATNative;
import com.anythink.nativead.api.ATNativeAdView;
import com.anythink.nativead.api.ATNativeDislikeListener;
import com.anythink.nativead.api.ATNativeEventListener;
import com.anythink.nativead.api.ATNativeNetworkListener;
import com.anythink.nativead.api.ATNativePrepareInfo;
import com.anythink.nativead.api.NativeAd;

public class NativeActivity extends AppCompatActivity {

    ATNative atNatives;
    ATNativeAdView anyThinkNativeAdView;
    NativeAd mNativeAd;

    FrameLayout adContainer;
    int adViewWidth = -1;
    int adViewHeight = -1;
    private String TAG = "NativeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native);
        adContainer = findViewById(R.id.fl_container);
        initNativeAd();
        loadNativeAd();
    }

    public void initNativeAd() {
        atNatives = new ATNative(this, "b61adcbdf7c1e2", new ATNativeNetworkListener() {

            @Override
            public void onNativeAdLoaded() {
                Log.i(TAG, "onNativeAdLoaded");
                showNativeAd();
            }

            @Override
            public void onNativeAdLoadFail(AdError adError) {
                //注意：禁止在此回调中执行广告的加载方法进行重试，否则会引起很多无用请求且可能会导致应用卡顿
                //AdError，请参考 https://docs.toponad.com/#/zh-cn/android/android_doc/android_test?id=aderror
                Log.i(TAG, "onNativeAdLoadFail:" + adError.getFullErrorInfo());
            }
        });


        if (anyThinkNativeAdView == null) {
            anyThinkNativeAdView = new ATNativeAdView(this);
        }
    }

    public void loadNativeAd() {

        if (atNatives != null) {
            atNatives.makeAdRequest();
        }
    }

    public void showNativeAd() {
        if (atNatives == null) {
            return;
        }


        NativeAd nativeAd = atNatives.getNativeAd();
        if (nativeAd != null) {
            if (anyThinkNativeAdView != null) {
                anyThinkNativeAdView.removeAllViews();
                //添加到您的布局中
                if (anyThinkNativeAdView.getParent() == null) {
                    adContainer.addView(anyThinkNativeAdView, new FrameLayout.LayoutParams(adViewWidth, adViewHeight));
                }
            }


            mNativeAd = nativeAd;
            mNativeAd.setDislikeCallbackListener(new ATNativeDislikeListener() {
                @Override
                public void onAdCloseButtonClick(ATNativeAdView atNativeAdView, ATAdInfo atAdInfo) {
                    Log.i(TAG, "native ad onAdCloseButtonClick:\n");
                    adContainer.removeAllViews();

                }
            });
            mNativeAd.setNativeEventListener(new ATNativeEventListener() {
                @Override
                public void onAdImpressed(ATNativeAdView view, ATAdInfo atAdInfo) {
                    //ATAdInfo可区分广告平台以及获取广告平台的广告位ID等
                    //请参考 https://docs.toponad.com/#/zh-cn/android/android_doc/android_sdk_callback_access?id=callback_info

                    Log.i(TAG, "native ad onAdImpressed:\n" + atAdInfo.toString());
                }

                @Override
                public void onAdClicked(ATNativeAdView view, ATAdInfo atAdInfo) {
                    Log.i(TAG, "native ad onAdClicked:\n" + atAdInfo.toString());
                }

                @Override
                public void onAdVideoStart(ATNativeAdView view) {
                    Log.i(TAG, "native ad onAdVideoStart");
                }

                @Override
                public void onAdVideoEnd(ATNativeAdView view) {
                    Log.i(TAG, "native ad onAdVideoEnd");
                }

                @Override
                public void onAdVideoProgress(ATNativeAdView view, int progress) {
                    Log.i(TAG, "native ad onAdVideoProgress:" + progress);
                }
            });


            ATNativePrepareInfo nativePrepareInfo = null;
            mNativeAd.renderAdContainer(anyThinkNativeAdView, null);
            mNativeAd.prepare(anyThinkNativeAdView, nativePrepareInfo);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mNativeAd != null) {
            mNativeAd.destory();
        }
    }

    @Override
    protected void onPause() {
        if (mNativeAd != null) {
            mNativeAd.onPause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mNativeAd != null) {
            mNativeAd.onResume();
        }
        super.onResume();
    }
}
