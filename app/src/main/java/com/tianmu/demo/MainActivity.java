package com.tianmu.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnSplash = findViewById(R.id.btn_splash);
        btnSplash.setOnClickListener(this);
        Button btnBanner = findViewById(R.id.btn_banner);
        btnBanner.setOnClickListener(this);
        Button btnInterstitial = findViewById(R.id.btn_interstitial);
        btnInterstitial.setOnClickListener(this);
        Button btnNative = findViewById(R.id.btn_native);
        btnNative.setOnClickListener(this);
        Button btnReward = findViewById(R.id.btn_reward);
        btnReward.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_splash:
                Intent intent = new Intent(MainActivity.this, SplashAdActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_banner:
                intent = new Intent(MainActivity.this, BannerAdActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_interstitial:
                intent = new Intent(MainActivity.this, InterstitialActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_native:
                intent = new Intent(MainActivity.this, NativeActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_reward:
                intent = new Intent(MainActivity.this, RewardActivity.class);
                startActivity(intent);
                break;
            default:
        }
    }
}
