package com.tianmu.demo;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.anythink.core.api.ATAdInfo;
import com.anythink.core.api.AdError;
import com.anythink.splashad.api.ATSplashAd;
import com.anythink.splashad.api.ATSplashAdExtraInfo;
import com.anythink.splashad.api.ATSplashAdListener;
import com.anythink.splashad.api.IATSplashEyeAd;

public class SplashAdActivity extends AppCompatActivity implements ATSplashAdListener {
    ATSplashAd splashAd;
    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_ad);


        container = findViewById(R.id.splash_ad_container);

        try {
            String unitId = "b639fde212ddb2";
            splashAd = new ATSplashAd(this, unitId, this);

//            Map<String, Object> localMap = new HashMap<>();
//            localMap.put(ATAdConst.KEY.AD_WIDTH, layoutParams.width);
//            localMap.put(ATAdConst.KEY.AD_HEIGHT, layoutParams.height);
//            splashAd.setLocalExtra(localMap);

            Log.i("SplashAdShowActivity", "SplashAd isn't ready to show, start to request.");
            splashAd.loadAd();


        } catch (Exception e) {
            Log.e("SplashAdShowActivity", e.toString());
        }

    }


    @Override
    public void onAdLoaded(boolean isTimeout) {
        Log.i("SplashAdShowActivity", "onAdLoaded---------");
        //开发者可在这里处理广告加载成功，但timeout的情况。如果本次加载的广告没有show，会保留到缓存中，供下次使用
        if (!isTimeout) {
            splashAd.show(this, container);
        } else {
            Log.i("SplashAdShowActivity", "onAdLoaded isTimeout");
        }
    }

    @Override
    public void onAdLoadTimeout() {
        //本次加载超过时间超过fetchAdTimeout（默认为5s）没有广告返回，开发者可在此回调处理开屏加载超时逻辑
        Log.i("SplashAdShowActivity", "SplashAd load timeout");
        jumpToMainActivity();
    }


    @Override
    public void onNoAdError(AdError adError) {
        Log.i("SplashAdShowActivity", "onNoAdError---------:" + adError.getFullErrorInfo());
        jumpToMainActivity();
    }

    @Override
    public void onAdShow(ATAdInfo entity) {
        Log.i("SplashAdShowActivity", "onAdShow:" + entity.toString());
    }

    @Override
    public void onAdClick(ATAdInfo entity) {
        Log.i("SplashAdShowActivity", "onAdClick:" + entity.toString());
    }

    @Override
    public void onAdDismiss(ATAdInfo atAdInfo, ATSplashAdExtraInfo atSplashAdExtraInfo) {
        jumpToMainActivity();
    }


    boolean hasHandleJump = false;
    boolean canJump;

    @Override
    protected void onResume() {
        super.onResume();

        if (canJump) {
            jumpToMainActivity();
        }

        canJump = true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        canJump = false;
    }

    public void jumpToMainActivity() {
        if (!canJump) {
            canJump = true;
            return;
        }

        if (!hasHandleJump) {
            hasHandleJump = true;
            finish();
            Toast.makeText(this, "start your MainActivity.", Toast.LENGTH_SHORT).show();
        }
        Toast.makeText(this, "start your MainActivity.", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (splashAd != null) {
            splashAd.onDestory();
        }

    }
}
