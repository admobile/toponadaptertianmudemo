package com.tianmu.demo;

import android.app.Application;
import android.os.Build;
import android.util.Log;
import android.webkit.WebView;

import com.anythink.core.api.ATSDK;
import com.anythink.core.api.ATSDKInitListener;
import com.tianmu.toponadaptertianmu.TianmuATCustomController;
import com.tianmu.toponadaptertianmu.TianmuATInitManager;

/**
 * @author songzi
 * @date 2021/12/6
 */
public class DemoApplicaion extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //Android 9 or above must be set
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            String processName = getProcessName();
            if (!getPackageName().equals(processName)) {
                WebView.setDataDirectorySuffix(processName);
            }
        }

        TianmuATInitManager.getInstance().setTMATCustomController(new TianmuATCustomController() {

            /**
             * 是否为Debug模式，开发阶段可设置为true
             *
             * @return true debug模式，false 非debug模式。默认为BuildConfig.DEBUG
             */
            @Override
            public boolean isDebug() {
                return BuildConfig.DEBUG;
            }

            /**
             * 用户是否同意隐私政策
             *
             * @return true同意，false不同意。默认为true
             */
            @Override
            public boolean isAgreePrivacyStrategy() {
                return true;
            }

            /**
             *  开发者可以传入oaid
             *  信通院OAID的相关采集——如何获取OAID：
             1. 移动安全联盟官网http://www.msa-alliance.cn/
             2. 信通院统一SDK下载http://msa-alliance.cn/col.jsp?id=120
             * @return oaid
             */
            @Override
            public String getOaid() {
                return null;
            }

            @Override
            public String geAndroidId() {
                return null;
            }

            @Override
            public String getMac() {
                return null;
            }

            @Override
            public String getImei() {
                return null;
            }

            /**
             * 是否允许天目SDK主动使用ACCESS_WIFI_STATE权限
             *
             * @return true可以使用，false禁止使用。默认为true
             */
            @Override
            public boolean isCanUseWifiState() {
                return true;
            }

            /**
             * 是否允许天目SDK主动使用手机硬件参数，如：imei
             *
             * @return true可以使用，false禁止使用。默认为true
             */
            @Override
            public boolean isCanUsePhoneState() {
                return true;
            }

            /**
             * 是否允许穿山甲SDK主动使用地理位置信息
             *
             * @return true可以获取，false禁止获取。默认为true
             */
            @Override
            public boolean isCanUseLocation() {
                return true;
            }
        });

        ATSDK.setNetworkLogDebug(true);
        ATSDK.integrationChecking(getApplicationContext());
        ATSDK.init(this, "a61adcb7f2ab48", "350cd0c42af3c5f2185b83bbdf015391");

    }

}
