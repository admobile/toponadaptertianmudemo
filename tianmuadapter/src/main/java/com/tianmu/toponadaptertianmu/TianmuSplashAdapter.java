package com.tianmu.toponadaptertianmu;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import com.anythink.core.api.ATAdConst;
import com.anythink.core.api.ATBiddingListener;
import com.anythink.core.api.ATBiddingResult;
import com.anythink.core.api.MediationInitCallback;
import com.anythink.splashad.unitgroup.api.CustomSplashAdapter;
import com.tianmu.ad.SplashAd;
import com.tianmu.ad.bean.SplashAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.SplashAdListener;

import java.util.Map;

/**
 * @author songzi
 * @date 2021/12/6
 */
public class TianmuSplashAdapter extends CustomSplashAdapter {
    private String slotId;
    private SplashAd splashAd;
    private boolean isBidding;
    private SplashAdInfo mSplashAdInfo;
    private ATBiddingListener biddingListener;

    @Override
    public void loadCustomNetworkAd(final Context context, Map<String, Object> serverExtra, Map<String, Object> localExtra) {
        if (serverExtra.containsKey("slot_id")) {
            slotId = (String) serverExtra.get("slot_id");
        }
        TianmuInitManager.getInstance().
                initSDK(context, serverExtra, new MediationInitCallback() {
                    @Override
                    public void onSuccess() {
                        String name = Thread.currentThread().getName();
                        Log.e("wsong", "name = " + name);
                        startLoad(context);
                    }

                    @Override
                    public void onFail(String msg) {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1001", msg);
                        }
                    }
                });
    }

    @Override
    public boolean startBiddingRequest(final Context context, Map<String, Object> serverExtra, Map<String, Object> localExtra, ATBiddingListener biddingListener) {
        this.isBidding = true;
        this.biddingListener = biddingListener;
        if (serverExtra.containsKey("slot_id")) {
            slotId = (String) serverExtra.get("slot_id");
        }

        TianmuInitManager.getInstance().
                initSDK(context, serverExtra, new MediationInitCallback() {
                    @Override
                    public void onSuccess() {
                        String name = Thread.currentThread().getName();
                        Log.e("wsong", "name = " + name);
                        startLoad(context);
                    }

                    @Override
                    public void onFail(String msg) {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1001", msg);
                        }
                    }
                });
        return true;
    }

    private void startLoad(final Context context) {
        postOnMainThread(new Runnable() {
            @Override
            public void run() {

                if (context == null) {
                    if (isBidding) {
                        if (biddingListener != null) {
                            biddingListener.onC2SBiddingResultWithCache(
                                    ATBiddingResult.fail("context is null"),
                                    null);
                        }
                    } else {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1", "context is null");
                        }
                    }
                    return;
                }

                splashAd = new SplashAd(context);
                splashAd.setListener(new SplashAdListener() {
                    @Override
                    public void onAdTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onAdReceive(SplashAdInfo adInfo) {
                        mSplashAdInfo = adInfo;
                        if (isBidding) {
                            if (biddingListener != null) {
                                if (adInfo != null && adInfo.getBidPrice() > 0) {
                                    biddingListener.onC2SBiddingResultWithCache(
                                            ATBiddingResult.success(
                                                    adInfo.getBidPrice()
                                                    , adInfo.getKey()
                                                    , null
                                                    , ATAdConst.CURRENCY.RMB_CENT)
                                            , null);
                                } else {
                                    biddingListener.onC2SBiddingResultWithCache(
                                            ATBiddingResult.fail("adInfo is null"),
                                            null);
                                }
                            }
                        } else {
                            if (mLoadListener != null) {
                                mLoadListener.onAdCacheLoaded();
                            }
                        }
                    }

                    @Override
                    public void onAdExpose(SplashAdInfo adInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onSplashAdShow();
                        }
                    }

                    @Override
                    public void onAdClick(SplashAdInfo adInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onSplashAdClicked();
                        }
                    }

                    @Override
                    public void onAdSkip(SplashAdInfo adInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onSplashAdDismiss();
                        }
                    }

                    @Override
                    public void onAdClose(SplashAdInfo adInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onSplashAdDismiss();
                        }
                    }

                    @Override
                    public void onAdFailed(TianmuError tianmuError) {
                        if (isBidding) {
                            if (biddingListener != null) {
                                biddingListener.onC2SBiddingResultWithCache(
                                        ATBiddingResult.fail(tianmuError.getError())
                                        , null
                                );
                            }
                        } else {
                            if (mLoadListener != null) {
                                mLoadListener.onAdLoadError(
                                        "" + tianmuError.getCode()
                                        , "" + tianmuError.getError()
                                );
                            }
                        }
                    }
                });
                splashAd.loadAd(getNetworkPlacementId());
            }
        });

    }

    @Override
    public void show(Activity activity, final ViewGroup container) {
        postOnMainThread(new Runnable() {
            @Override
            public void run() {
                if (splashAd != null && mSplashAdInfo != null) {
                    container.removeAllViews();
                    container.addView(mSplashAdInfo.getSplashAdView());
                    mSplashAdInfo.render();
                }
            }
        });
    }

    @Override
    public String getNetworkPlacementId() {
        return slotId;
    }

    @Override
    public String getNetworkSDKVersion() {
        return TianmuInitManager.getInstance().getNetworkVersion();
    }

    @Override
    public String getNetworkName() {
        return TianmuInitManager.getInstance().getNetworkName();
    }

    @Override
    public boolean isAdReady() {
        return splashAd != null;
    }

    @Override
    public void destory() {
        if (splashAd != null) {
            splashAd.release();
        }

        biddingListener = null;
    }
}
