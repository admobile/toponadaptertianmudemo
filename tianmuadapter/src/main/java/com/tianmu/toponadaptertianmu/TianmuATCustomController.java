package com.tianmu.toponadaptertianmu;

/**
 * @author maipian
 * @description 描述
 * @date 12/9/24
 */
public class TianmuATCustomController {

    /**
     * 是否为Debug模式，开发阶段可设置为true
     *
     * @return true debug模式，false 非debug模式。默认为true
     */
    public boolean isDebug() {
        return true;
    }
    /**
     * 用户是否同意隐私政策
     *
     * @return true同意，false不同意。默认为true
     */
    public boolean isAgreePrivacyStrategy() {
        return true;
    }
    /**
     *  开发者可以传入oaid
     *  信通院OAID的相关采集——如何获取OAID：
     1. 移动安全联盟官网http://www.msa-alliance.cn/
     2. 信通院统一SDK下载http://msa-alliance.cn/col.jsp?id=120
     * @return oaid
     */
    public String getOaid() {
        return "";
    }

    /**
     * @return AndroidId 信息
     */
    public String geAndroidId() {
        return "";
    }

    /**
     * @return Mac 信息
     */
    public String getMac() {
        return "";
    }

    /**
     * @return Imei 信息
     */
    public String getImei() {
        return "";
    }

    /**
     * 是否允许天目SDK主动使用ACCESS_WIFI_STATE权限
     *
     * @return true可以使用，false禁止使用。默认为true
     */
    public boolean isCanUseWifiState() {
        return true;
    }

    /**
     * 是否允许天目SDK主动使用手机硬件参数，如：imei
     *
     * @return true可以使用，false禁止使用。默认为true
     */
    public boolean isCanUsePhoneState() {
        return true;
    }

    /**
     * 是否允许穿山甲SDK主动使用地理位置信息
     *
     * @return true可以获取，false禁止获取。默认为true
     */
    public boolean isCanUseLocation() {
        return true;
    }

}
