package com.tianmu.toponadaptertianmu;

/**
 * @author maipian
 * @description 描述
 * @date 12/9/24
 */
public class TianmuATInitManager {
    private static TianmuATInitManager instance;

    private TianmuATCustomController tmAtCustomController = new TianmuATCustomController();

    public static TianmuATInitManager getInstance() {
        if (instance == null) {
            synchronized (TianmuATInitManager.class) {
                if (instance == null) {
                    instance = new TianmuATInitManager();
                }
            }
        }
        return instance;
    }

    public void setTMATCustomController(TianmuATCustomController tmAtCustomController) {
        this.tmAtCustomController = tmAtCustomController;
    }

    public String getOaid() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.getOaid();
        }
        return "";
    }

    public String getVaid() {
        return "";
    }

    public String geAndroidId() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.geAndroidId();
        }
        return "";
    }

    public String getMac() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.getMac();
        }
        return "";
    }

    public String getImei() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.getImei();
        }
        return "";
    }

    public boolean isDebug() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.isDebug();
        }
        return true;
    }

    public boolean isCanUseWifiState() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.isCanUseWifiState();
        }
        return true;
    }

    public boolean isCanUsePhoneState() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.isCanUsePhoneState();
        }
        return true;
    }

    public boolean isCanUseLocation() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.isCanUseLocation();
        }
        return true;
    }

    public boolean isAgreePrivacyStrategy() {
        if (tmAtCustomController != null) {
            return tmAtCustomController.isAgreePrivacyStrategy();
        }
        return true;
    }

}
