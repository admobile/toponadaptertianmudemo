package com.tianmu.toponadaptertianmu;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.anythink.core.api.ATInitMediation;
import com.anythink.core.api.MediationInitCallback;
import com.tianmu.TianmuSDK;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.config.TianmuCustomController;
import com.tianmu.config.TianmuInitConfig;
import com.tianmu.listener.TianmuInitListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class TianmuInitManager extends ATInitMediation {
    private static TianmuInitManager sInstance;
    private Handler mHandler;
    private boolean mIsOpenDirectDownload;

    private boolean mHasInit;
    private AtomicBoolean mIsIniting;
    private List<MediationInitCallback> mListeners;
    private final Object mLock = new Object();

    private TianmuInitManager() {
        mHandler = new Handler(Looper.getMainLooper());
        mIsOpenDirectDownload = false;
        mIsIniting = new AtomicBoolean(false);
    }

    public synchronized static TianmuInitManager getInstance() {
        if (sInstance == null) {
            sInstance = new TianmuInitManager();
        }
        return sInstance;
    }


    @Override
    public synchronized void initSDK(Context context, Map<String, Object> serviceExtras, MediationInitCallback mediationInitCallback) {
        initTMSDK(context, serviceExtras, mediationInitCallback);
    }


    public void initTMSDK(final Context context, Map<String, Object> serviceExtras, final MediationInitCallback callback) {

        if (mHasInit) {
            if (callback != null) {
                callback.onSuccess();
            }
            return;
        }

        synchronized (mLock) {

            if (mIsIniting.get()) {
                if (callback != null) {
                    mListeners.add(callback);
                }
                return;
            }

            if (mListeners == null) {
                mListeners = new ArrayList<>();
            }

            mIsIniting.set(true);
        }

        if (!serviceExtras.containsKey("app_id")) {
            callback.onFail("app_id is null");
            return;
        }

        if (!serviceExtras.containsKey("slot_id")) {
            callback.onFail("slot_id is null");
            return;
        }

        final String appId = (String) serviceExtras.get("app_id");

        if (callback != null) {
            mListeners.add(callback);
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                initTianmuAd(context, appId);
            }
        });
    }

    private void initTianmuAd(Context context, String appId) {

//        Log.d("TianmuLog", "isDebug: " + TianmuATInitManager.getInstance().isDebug());
//        Log.d("TianmuLog", "isAgreePrivacyStrategy: " + TianmuATInitManager.getInstance().isAgreePrivacyStrategy());
//        Log.d("TianmuLog", "isCanUseWifiState: " + TianmuATInitManager.getInstance().isCanUseWifiState());
//        Log.d("TianmuLog", "isCanUsePhoneState: " + TianmuATInitManager.getInstance().isCanUsePhoneState());
//        Log.d("TianmuLog", "isCanUseLocation: " + TianmuATInitManager.getInstance().isCanUseLocation());
//        Log.d("TianmuLog", "getOaid: " + TianmuATInitManager.getInstance().getOaid());
//        Log.d("TianmuLog", "geAndroidId: " + TianmuATInitManager.getInstance().geAndroidId());
//        Log.d("TianmuLog", "getMac: " + TianmuATInitManager.getInstance().getMac());
//        Log.d("TianmuLog", "getImei: " + TianmuATInitManager.getInstance().getImei());

        try {
            TianmuSDK.getInstance().init(context, new TianmuInitConfig.Builder()
                    .appId(appId)
                    .debug(TianmuATInitManager.getInstance().isDebug())
                    .agreePrivacyStrategy(TianmuATInitManager.getInstance().isAgreePrivacyStrategy())
                    .isCanUseWifiState(TianmuATInitManager.getInstance().isCanUseWifiState())
                    .isCanUsePhoneState(TianmuATInitManager.getInstance().isCanUsePhoneState())
                    .isCanUseLocation(TianmuATInitManager.getInstance().isCanUseLocation())
                    .setTianmuCustomController(new TianmuCustomController() {
                        @Override
                        public String getDevOaid() {
                            return TianmuATInitManager.getInstance().getOaid();
                        }

                        @Override
                        public String getDevVaid() {
                            return TianmuATInitManager.getInstance().getVaid();
                        }

                        @Override
                        public String getAndroidId() {
                            return TianmuATInitManager.getInstance().geAndroidId();
                        }

                        @Override
                        public String getMacAddress() {
                            return TianmuATInitManager.getInstance().getMac();
                        }

                        @Override
                        public String getDevImei() {
                            return TianmuATInitManager.getInstance().getImei();
                        }
                    })
                    .build(), new TianmuInitListener() {
                @Override
                public void onInitFinished() {
                    mHasInit = true;
                    callbackResult(true, null, null);
                }

                @Override
                public void onInitFailed(TianmuError tianmuError) {
                    callbackResult(false, tianmuError.getCode() + "", tianmuError.getError());
                }
            });
        } catch (Throwable e) {
            callbackResult(false, "", e.getMessage());
        }
    }

    private void callbackResult(boolean success, String errorCode, String errorMsg) {
        synchronized (mLock) {
            int size = mListeners.size();
            MediationInitCallback initListener;
            for (int i = 0; i < size; i++) {
                initListener = mListeners.get(i);
                if (initListener != null) {
                    if (success) {
                        initListener.onSuccess();
                    } else {
                        initListener.onFail(errorCode + ":" + errorMsg);
                    }
                }
            }
            mListeners.clear();

            mIsIniting.set(false);
        }
    }

    @Override
    public String getNetworkName() {
        return "tianmu";
    }


    @Override
    public String getNetworkVersion() {
        try {
            return TianmuSDK.getInstance().getSdkVersion();
        } catch (Throwable e) {

        }
        return "";
    }
}
