package com.tianmu.toponadaptertianmu;

import android.app.Activity;
import android.content.Context;

import com.anythink.core.api.ATAdConst;
import com.anythink.core.api.ATBiddingListener;
import com.anythink.core.api.ATBiddingResult;
import com.anythink.core.api.MediationInitCallback;
import com.anythink.rewardvideo.unitgroup.api.CustomRewardVideoAdapter;
import com.tianmu.ad.RewardAd;
import com.tianmu.ad.bean.RewardAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.RewardAdListener;

import java.util.Map;

/**
 * @Description:
 * @Author: maipian
 * @CreateDate: 2021/12/7 2:58 PM
 */
public class TianmuRewardAdapter extends CustomRewardVideoAdapter {
    private RewardAd rewardAd;
    private String slotId;
    private boolean isBidding;
    private RewardAdInfo mRewardAdInfo;
    private ATBiddingListener biddingListener;

    @Override
    public void loadCustomNetworkAd(final Context context, Map<String, Object> serverExtra, Map<String, Object> localExtra) {
        if (serverExtra.containsKey("slot_id")) {
            slotId = (String) serverExtra.get("slot_id");
        }
        TianmuInitManager.getInstance().
                initSDK(context, serverExtra, new MediationInitCallback() {
                    @Override
                    public void onSuccess() {
                        startLoad(context);
                    }

                    @Override
                    public void onFail(String s) {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1001", s);
                        }
                    }
                });
    }

    @Override
    public boolean startBiddingRequest(final Context context, Map<String, Object> serverExtra, Map<String, Object> localExtra, ATBiddingListener biddingListener) {
        this.isBidding = true;
        this.biddingListener = biddingListener;
        if (serverExtra.containsKey("slot_id")) {
            slotId = (String) serverExtra.get("slot_id");
        }

        TianmuInitManager.getInstance().
                initSDK(context, serverExtra, new MediationInitCallback() {
                    @Override
                    public void onSuccess() {
                        startLoad(context);
                    }

                    @Override
                    public void onFail(String s) {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1001", s);
                        }
                    }
                });
        return true;
    }

    private void startLoad(final Context context) {
        postOnMainThread(new Runnable() {
            @Override
            public void run() {

                if (context == null) {
                    if (isBidding) {
                        if (biddingListener != null) {
                            biddingListener.onC2SBiddingResultWithCache(
                                    ATBiddingResult.fail("context is null"),
                                    null);
                        }
                    } else {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1", "context is null");
                        }
                    }
                    return;
                }

                rewardAd = new RewardAd(context);
                rewardAd.setListener(new RewardAdListener() {

                    @Override
                    public void onAdReceive(RewardAdInfo adInfo) {
                        mRewardAdInfo = adInfo;
                        if (isBidding) {
                            if (biddingListener != null) {
                                if (adInfo != null && adInfo.getBidPrice() > 0) {
                                    biddingListener.onC2SBiddingResultWithCache(
                                            ATBiddingResult.success(
                                                    adInfo.getBidPrice()
                                                    , adInfo.getKey()
                                                    , null
                                                    , ATAdConst.CURRENCY.RMB_CENT)
                                            , null);
                                } else {
                                    biddingListener.onC2SBiddingResultWithCache(
                                            ATBiddingResult.fail("adInfo is null"),
                                            null);
                                }
                            }
                        } else {
                            if (mLoadListener != null) {
                                mLoadListener.onAdCacheLoaded();
                            }
                        }
                    }

                    @Override
                    public void onAdClick(RewardAdInfo rewardAdInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onRewardedVideoAdPlayClicked();
                        }
                    }

                    @Override
                    public void onAdClose(RewardAdInfo rewardAdInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onRewardedVideoAdClosed();
                        }
                    }

                    @Override
                    public void onAdExpose(RewardAdInfo rewardAdInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onRewardedVideoAdPlayStart();
                        }
                    }

                    @Override
                    public void onAdReward(RewardAdInfo rewardAdInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onReward();
                        }
                    }

                    @Override
                    public void onVideoCompleted(RewardAdInfo rewardAdInfo) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onRewardedVideoAdPlayEnd();
                        }
                    }

                    @Override
                    public void onVideoError(RewardAdInfo rewardAdInfo, String msg) {
                        if (mImpressionListener != null) {
                            mImpressionListener.onRewardedVideoAdPlayFailed("-1", msg);
                        }
                    }

                    @Override
                    public void onVideoSkip(RewardAdInfo rewardAdInfo) {

                    }

                    @Override
                    public void onAdFailed(TianmuError tianmuError) {
                        if (isBidding) {
                            if (biddingListener != null) {
                                biddingListener.onC2SBiddingResultWithCache(
                                        ATBiddingResult.fail(tianmuError.getError())
                                        , null
                                );
                            }
                        } else {
                            if (mLoadListener != null) {
                                mLoadListener.onAdLoadError(
                                        "" + tianmuError.getCode()
                                        , "" + tianmuError.getError()
                                );
                            }
                        }
                    }
                });
                rewardAd.loadAd(getNetworkPlacementId());
            }
        });
    }

    @Override
    public void show(Activity activity) {
        if (isAdReady()) {
            mRewardAdInfo.showRewardAd(activity);
        }
    }

    @Override
    public String getNetworkPlacementId() {
        return slotId;
    }

    @Override
    public String getNetworkSDKVersion() {
        return TianmuInitManager.getInstance().getNetworkVersion();
    }

    @Override
    public String getNetworkName() {
        return TianmuInitManager.getInstance().getNetworkName();
    }

    @Override
    public boolean isAdReady() {
        return mRewardAdInfo != null;
    }

    @Override
    public void destory() {
        if (rewardAd != null) {
            rewardAd.release();
            rewardAd = null;
        }

        if (mRewardAdInfo != null) {
            mRewardAdInfo.release();
            mRewardAdInfo = null;
        }

        biddingListener = null;
    }
}
