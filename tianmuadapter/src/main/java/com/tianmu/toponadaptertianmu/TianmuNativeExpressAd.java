package com.tianmu.toponadaptertianmu;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.anythink.nativead.api.ATNativePrepareInfo;
import com.anythink.nativead.unitgroup.api.CustomNativeAd;
import com.tianmu.ad.bean.NativeExpressAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.NativeExpressAdListener;

import java.util.List;

/**
 * @Description:
 * @Author: maipian
 * @CreateDate: 2021/12/7 5:56 PM
 */
public class TianmuNativeExpressAd extends CustomNativeAd implements NativeExpressAdListener {
    private NativeExpressAdInfo nativeExpressAdInfo;
    private View mMediaView;
    private boolean isBidding;

    public TianmuNativeExpressAd(Context context, NativeExpressAdInfo nativeExpressAdInfo, boolean isBidding) {
        this.nativeExpressAdInfo = nativeExpressAdInfo;
        this.isBidding = isBidding;
    }

    @Override
    public boolean isNativeExpress() {
        return true;
    }

    @Override
    public View getAdMediaView(Object... object) {
        try {
            if (mMediaView == null && nativeExpressAdInfo != null && object[0] != null && object[0] instanceof ViewGroup) {
                ViewGroup container = (ViewGroup) object[0];
                container.removeAllViews();
                mMediaView = nativeExpressAdInfo.getNativeExpressAdView();
            }
            return mMediaView;
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public void prepare(View view, ATNativePrepareInfo nativePrepareInfo) {
        super.prepare(view, nativePrepareInfo);
        renderExpressAd();
    }

    public void renderExpressAd() {
        if (nativeExpressAdInfo != null) {
            nativeExpressAdInfo.render();
        }
    }

    @Override
    public void destroy() {
        mMediaView = null;
        if (nativeExpressAdInfo != null) {
            nativeExpressAdInfo.release();
            nativeExpressAdInfo = null;
        }
    }

    @Override
    public void onRenderFailed(NativeExpressAdInfo nativeExpressAdInfo, TianmuError tianmuError) {

    }

    @Override
    public void onAdReceive(List<NativeExpressAdInfo> list) {

    }

    @Override
    public void onAdExpose(NativeExpressAdInfo nativeExpressAdInfo) {
        notifyAdImpression();
    }

    @Override
    public void onAdClick(NativeExpressAdInfo nativeExpressAdInfo) {
        notifyAdClicked();
    }

    @Override
    public void onAdClose(NativeExpressAdInfo nativeExpressAdInfo) {
        notifyAdDislikeClick();
    }

    @Override
    public void onAdFailed(TianmuError tianmuError) {

    }


}
