package com.tianmu.toponadaptertianmu;

import android.app.Activity;
import android.content.Context;

import com.anythink.core.api.ATAdConst;
import com.anythink.core.api.ATBiddingListener;
import com.anythink.core.api.ATBiddingResult;
import com.anythink.core.api.MediationInitCallback;
import com.anythink.interstitial.unitgroup.api.CustomInterstitialAdapter;
import com.tianmu.ad.InterstitialAd;
import com.tianmu.ad.bean.InterstitialAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.InterstitialAdListener;

import java.util.Map;

/**
 * @Description:
 * @Author: maipian
 * @CreateDate: 2021/12/7 2:58 PM
 */
public class TianmuInterstitialAdapter extends CustomInterstitialAdapter {
    private InterstitialAd interstitialAd;
    private String slotId;
    private boolean isBidding;
    private InterstitialAdInfo mInterstitialAdInfo;
    private ATBiddingListener biddingListener;

    @Override
    public void loadCustomNetworkAd(final Context context, Map<String, Object> serverExtra, Map<String, Object> localExtra) {
        if (serverExtra.containsKey("slot_id")) {
            slotId = (String) serverExtra.get("slot_id");
        }
        TianmuInitManager.getInstance().
                initSDK(context, serverExtra, new MediationInitCallback() {
                    @Override
                    public void onSuccess() {
                        startLoad(context);
                    }

                    @Override
                    public void onFail(String s) {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1001", s);
                        }
                    }
                });

    }

    @Override
    public boolean startBiddingRequest(final Context context, Map<String, Object> serverExtra, Map<String, Object> localExtra, ATBiddingListener biddingListener) {
        this.isBidding = true;
        this.biddingListener = biddingListener;
        if (serverExtra.containsKey("slot_id")) {
            slotId = (String) serverExtra.get("slot_id");
        }
        TianmuInitManager.getInstance().
                initSDK(context, serverExtra, new MediationInitCallback() {
                    @Override
                    public void onSuccess() {
                        startLoad(context);
                    }

                    @Override
                    public void onFail(String s) {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1001", s);
                        }
                    }
                });
        return true;
    }

    private void startLoad(final Context context) {
        postOnMainThread(new Runnable() {
            @Override
            public void run() {

                if (context == null) {
                    if (isBidding) {
                        if (biddingListener != null) {
                            biddingListener.onC2SBiddingResultWithCache(
                                    ATBiddingResult.fail("context is null"),
                                    null);
                        }
                    } else {
                        if (mLoadListener != null) {
                            mLoadListener.onAdLoadError("-1", "context is null");
                        }
                    }
                    return;
                }

                interstitialAd = new InterstitialAd(context);
                interstitialAd.setListener(new InterstitialAdListener() {
                    @Override
                    public void onVideoError(InterstitialAdInfo interstitialAdInfo) {

                    }

                    @Override
                    public void onVideoFinish(InterstitialAdInfo interstitialAdInfo) {

                    }

                    @Override
                    public void onVideoPause(InterstitialAdInfo interstitialAdInfo) {

                    }

                    @Override
                    public void onVideoStart(InterstitialAdInfo interstitialAdInfo) {

                    }

                    @Override
                    public void onAdReceive(InterstitialAdInfo adInfo) {
                        mInterstitialAdInfo = adInfo;
                        if (isBidding) {
                            if (biddingListener != null) {
                                if (adInfo != null && adInfo.getBidPrice() > 0) {
                                    biddingListener.onC2SBiddingResultWithCache(
                                            ATBiddingResult.success(
                                                    adInfo.getBidPrice()
                                                    , adInfo.getKey()
                                                    , null
                                                    , ATAdConst.CURRENCY.RMB_CENT
                                            ),
                                            null
                                    );
                                } else {
                                    biddingListener.onC2SBiddingResultWithCache(
                                            ATBiddingResult.fail("adInfo is null")
                                            , null
                                    );
                                }
                            }
                        } else {
                            if (mLoadListener != null) {
                                mLoadListener.onAdCacheLoaded();
                            }
                        }
                    }

                    @Override
                    public void onAdExpose(InterstitialAdInfo adInfo) {
                        if (mImpressListener != null) {
                            mImpressListener.onInterstitialAdShow();
                        }
                    }

                    @Override
                    public void onAdClick(InterstitialAdInfo adInfo) {
                        if (mImpressListener != null) {
                            mImpressListener.onInterstitialAdClicked();
                        }
                    }

                    @Override
                    public void onAdClose(InterstitialAdInfo adInfo) {
                        if (mImpressListener != null) {
                            mImpressListener.onInterstitialAdClose();
                        }
                    }

                    @Override
                    public void onAdFailed(TianmuError tianmuError) {
                        if (isBidding) {
                            if (biddingListener != null) {
                                biddingListener.onC2SBiddingResultWithCache(
                                        ATBiddingResult.fail(tianmuError.getError()),
                                        null);
                            }
                        } else {
                            if (mLoadListener != null) {
                                mLoadListener.onAdLoadError("" + tianmuError.getCode(), "" + tianmuError.getError());
                            }
                        }
                    }
                });
                interstitialAd.loadAd(getNetworkPlacementId());
            }
        });


    }

    @Override
    public void show(Activity activity) {
        if (isAdReady()) {
            mInterstitialAdInfo.showInterstitial(activity);
        }
    }

    @Override
    public boolean isAdReady() {
        return mInterstitialAdInfo != null;
    }

    @Override
    public String getNetworkPlacementId() {
        return slotId;
    }

    @Override
    public String getNetworkSDKVersion() {
        return TianmuInitManager.getInstance().getNetworkVersion();
    }

    @Override
    public String getNetworkName() {
        return TianmuInitManager.getInstance().getNetworkName();
    }

    @Override
    public void destory() {
        if (interstitialAd != null) {
            interstitialAd.release();
            interstitialAd = null;
        }

        if (mInterstitialAdInfo != null) {
            mInterstitialAdInfo.release();
            mInterstitialAdInfo = null;
        }

        biddingListener = null;
    }

}
