# Tianmu Topon Adatpter Android Sdk——接入文档

## 1. 概述
### 1.1 概述

尊敬的开发者朋友，欢迎您使用Tianmu广告SDK。通过本文档，您可以快速完成将天目接入Topon。

**注意：本SDK仅支持中国大陆地区**；如需发布到Google Play，请勿引入本SDK及相关依赖文件。

## 2. SDK集成

### 2.1 添加仓库
```java
allprojects {
    repositories {
        ...
        // 添加以下仓库地址
        maven { url "https://maven.admobile.top/repository/maven-releases/" }
        ...
    }
}
```

### 2.2 添加混淆
```java
# 天目
-keep class com.tianmu.**{ *; }
-keep class tianmu.com.** { *; }
-keep interface tianmu.com.** { *; }
```

### 2.3 添加依赖
```java
// 天目文档https://gitee.com/admobile/tianmu-advertising-sdk-android
implementation 'cn.admobiletop.adsuyi.ad.third:tianmu:2.3.0.1'

// 天目适配topon适配器，根据topon自定义广告平台编写
// topon自定义广告平台文档https://docs.toponad.com/#/zh-cn/android/NetworkAccess/customnetwork/customnetwork
implementation project(':tianmuadapter')
```

### 2.4 架构
```java
ndk {
    // 设置支持的SO库架构，暂不支持x86
    abiFilters 'armeabi-v7a', 'arm64-v8a'
}
```

## 3. topon后台设置

### 3.1 路径
```java
激励视频：com.tianmu.toponadaptertianmu.TianmuRewardAdapter
插屏：com.tianmu.toponadaptertianmu.TianmuInterstitialAdapter
横幅：com.tianmu.toponadaptertianmu.TianmuBannerAdapter
原生（信息流模版）：com.tianmu.toponadaptertianmu.TianmuNativeAdapter
开屏：com.tianmu.toponadaptertianmu.TianmuSplashAdapter
```

### 3.1 编辑广告源参数

```java
{"app_id":"天目appid","slot_id":"广告位"}
```

## 4. 设置天目隐私信息控制开关

#### 参考资料：[topon隐私信息控制开关](https://newdocs.toponad.com/docs/wNPGmZ?search=1#520f0ace6a9cfad9cffeab1c8bab749f)。

```java
TianmuATInitManager.getInstance().setTMATCustomController(new TianmuATCustomController() {

    /**
     * 是否为Debug模式，开发阶段可设置为true
     *
     * @return true debug模式，false 非debug模式。默认为true
     */
    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    /**
     * 用户是否同意隐私政策
     *
     * @return true同意，false不同意。默认为true
     */
    @Override
    public boolean isAgreePrivacyStrategy() {
        return true;
    }

    /**
     *  开发者可以传入oaid
     *  信通院OAID的相关采集——如何获取OAID：
     1. 移动安全联盟官网http://www.msa-alliance.cn/
     2. 信通院统一SDK下载http://msa-alliance.cn/col.jsp?id=120
     * @return oaid
     */
    @Override
    public String getOaid() {
        return null;
    }

    /**
     * @return AndroidId 信息
     */
    @Override
    public String geAndroidId() {
        return null;
    }

    /**
     * @return Mac 信息
     */
    @Override
    public String getMac() {
        return null;
    }

    /**
     * @return Imei 信息
     */
    @Override
    public String getImei() {
        return null;
    }

    /**
     * 是否允许天目SDK主动使用ACCESS_WIFI_STATE权限
     *
     * @return true可以使用，false禁止使用。默认为true
     */
    @Override
    public boolean isCanUseWifiState() {
        return true;
    }

    /**
     * 是否允许天目SDK主动使用手机硬件参数，如：imei
     *
     * @return true可以使用，false禁止使用。默认为true
     */
    @Override
    public boolean isCanUsePhoneState() {
        return true;
    }

    /**
     * 是否允许穿山甲SDK主动使用地理位置信息
     *
     * @return true可以获取，false禁止获取。默认为true
     */
    @Override
    public boolean isCanUseLocation() {
        return true;
    }
});
```

## 5. 注意

##### 请提供正确的正式和测试用SHA1值，保证天目初始化正常。

##### isDebug回传true时，可通过在编译器中输入TianmuLog，查看天目渠道异常信息。

## 6. 关于Client Bidding
当前版本天目Adapter，已对接Topon Client Bidding功能，可以参考demo。

## 7. Topon版本6.2.71



